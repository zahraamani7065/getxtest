import 'package:get/get.dart';
import 'package:practice_getx/detail/Controller/DetailController.dart';

class DetailBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(DetailController());
  }

}