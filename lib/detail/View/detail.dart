import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practice_getx/Services/ApiService.dart';
import 'package:practice_getx/home/Controller/HomeController.dart';

class DetailScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final apiService=Get.find<ApiService>();
    final homeController=Get.find<HomeController>();
   return Scaffold(
     appBar: AppBar(title: Text("Detail screen"),
     leading: IconButton(
       icon: const Icon(Icons.arrow_back),
       onPressed: (){
         Get.back();
         print(apiService.fetchTextFromApi()+"    ");

       },
     ),

     ),
     body: Center(
       child:
       Text ('${homeController.count}',style: TextStyle(fontSize: 24),),
     ),
   );
  }

}