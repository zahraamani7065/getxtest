import 'package:get/get.dart';

import '../../Services/ApiService.dart';
import '../Controller/HomeController.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
     Get.put(ApiService());
     Get.put(HomeController());
  }

}