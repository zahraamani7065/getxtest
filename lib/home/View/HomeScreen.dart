import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practice_getx/Services/ApiService.dart';
import 'package:practice_getx/detail/View/detail.dart';
import 'package:practice_getx/home/Controller/HomeController.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final apiService=Get.find<ApiService>();
    final homeController=Get.find<HomeController>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home Screen"),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() =>
              Text(
                "${homeController.count}",
                style: TextStyle(fontSize: 24),
              ),
            ),

            ElevatedButton(
              onPressed: () {
                Get.toNamed("/detail");
                print(apiService.fetchTextFromApi());
              },
              child: Text("go to detail"),

            ),
            ElevatedButton(
              onPressed: () {

                print(apiService.fetchTextFromApi());
                homeController.increament();
              },
              child: const Text("increment"),

            )
          ],
        ),
      ),
    );
  }
}
