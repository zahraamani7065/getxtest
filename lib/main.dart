import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practice_getx/detail/View/detail.dart';
import 'package:practice_getx/detail/binding/DetailBinding.dart';
import 'package:practice_getx/home/binding/HomeBinding.dart';

import 'home/View/HomeScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      getPages: [
        GetPage(name: "/home", page:()=> HomeScreen(),binding:HomeBinding() ),
        GetPage(name: "/detail", page: ()=>DetailScreen(),binding: DetailBinding())
      ],
      initialRoute : "/home",
    );
  }
}


